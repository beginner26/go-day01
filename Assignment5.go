package main

import "fmt"

func main() {

	var tambah int = 5 + 5
	var kurang int = 5 - 5
	var kali int = 5 * 5
	var bagi int = 5 / 5
	var hitung = 2 + (3 * 5)

	fmt.Println("Pertambahan")
	fmt.Println("5 + 5 = ", tambah)
	fmt.Println("Pengurangan")
	fmt.Println("5 - 5 = ", kurang)
	fmt.Println("Perkalian")
	fmt.Println("5 * 5 = ", kali)
	fmt.Println("Pembagian")
	fmt.Println("5 / 5 = ", bagi)
	fmt.Println("Perhitungan")
	fmt.Println("2 + 3 * 5 = ", hitung)

}
