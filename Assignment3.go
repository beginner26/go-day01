package main

import "fmt"

func main() {

	var a uint8 = 5
	var b uint16 = 6
	var c int8 = -5
	var d int16 = -6

	var e float32 = -5.5
	var f float64 = -6.6

	var g string = "foo"

	fmt.Printf("a = %d is of type %T \n", a, a)
	fmt.Printf("b = %d is of type %T \n", b, b)
	fmt.Printf("c = %d is of type %T \n", c, c)
	fmt.Printf("d = %d is of type %T \n\n", d, d)
	fmt.Printf("e = %.1f is of type %T \n", e, e)
	fmt.Printf("f = %.1f is of type %T \n\n", f, f)
	fmt.Printf("g = %s is of type %T \n", g, g)

}
